from distutils.core import setup, Extension
setup(name="geoalg", version="1.0",
        ext_modules=[
            Extension("geoalg", 
                ["geoalg.c", "Graham.c", "point.c", "liste_point.c"])
            ])
