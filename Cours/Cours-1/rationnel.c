/* rationnel.c */

#include <stdio.h> /* pour printf */
#include <assert.h> /* pour assert */
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

/* Constructeur. 
   Initialise A avec la fraction n/d.
   Le dénominateur d est non nul.
 */

void init_rationnel (struct rationnel* A, int n, int d)
{   int g;

    assert (d != 0);

    g = Euclide (n, d);
    A->numer = n / g;
    A->denom = d / g;
    if (A->denom < 0)
    {   A->numer = - A->numer;
        A->denom = - A->denom;
    }
}

/* Destructeur */

void clear_rationnel (struct rationnel* A)
{
}

/* Affecte à S (mode R) le rationnel A + B (A, B mode D) */
void add_rationnel
            (struct rationnel* S, struct rationnel* A, struct rationnel* B)
{
    int n, d;
    n = A->numer * B->denom + A->denom * B->numer;
    d = A->denom * B->denom;
    init_rationnel (S, n, d);
}

void print_rationnel (struct rationnel* A)
{
    if (A->denom == 1)
        printf ("%d\n", A->numer);
    else
        printf ("%d/%d\n", A->numer, A->denom);
}

