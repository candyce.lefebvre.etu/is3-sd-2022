import numpy as np
from dictionnaire import abr

# Chargement du fichier 'Esperanto-Francais.utf8'

table = np.loadtxt('Esperanto-Francais.utf8', encoding='utf8', 
                    delimiter=':', dtype='str')

# Utilisation des dictionnaires du langage Python

D = dict(table)
print ('ŝtrumpetoj appartient-il à D ?', 'ŝtrumpetoj' in D)
print ('zumi appartient-il à D ?', 'zumi' in D)
print ('traduction de zumi =', D['zumi'])
print ('Ajout de ŝtrumpetoj au dictionnaire')
D['ŝtrumpetoj'] = 'chaussettes'
print ('traduction de ŝtrumpetoj =', D['ŝtrumpetoj'])

# Mêmes opérations mais avec notre implantation des ABR

A = abr ()
A.ajouter (table)
print ('hauteur de A =', A.hauteur ())
print ('ŝtrumpetoj appartient-il à A ?', 
                not A.rechercher ('ŝtrumpetoj') is None)
print ('zumi appartient-il à A ?', not A.rechercher ('zumi') is None)
print ('traduction de zumi =', A.rechercher ('zumi'))
print ('Ajout de ŝtrumpetoj au dictionnaire')
table = np.array ([['ŝtrumpetoj', 'chaussettes']])
A.ajouter (table)
print ('traduction de ŝtrumpetoj =', A.rechercher ('ŝtrumpetoj'))

